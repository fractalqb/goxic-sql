package gxsql

import (
	"testing"

	"git.fractalqb.de/fractalqb/goxic"
)

const exampleTemplate = `SELECT * FROM :schema:users
		 WHERE name = :$nm: AND age < :$age: OR given = :$nm:`

func TestNotTemplate(t *testing.T) {
	var stmt Statement
	rew := PositionRewriter("?")
	err := stmt.Rewrite(
		rew,
		`SELECT id, newat, lasta, salt FROM %susers`,
		map[string]goxic.Content{
			"schema": goxic.Empty,
			"autoid": goxic.Empty,
		})
	if err != nil {
		t.Fatal(err)
	}
}
