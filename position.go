package gxsql

import (
	"database/sql"
	"fmt"

	"git.fractalqb.de/fractalqb/goxic"
	"git.fractalqb.de/fractalqb/goxic/content"
)

type PositionBinder []string

func (pb PositionBinder) Bind(args ...sql.NamedArg) ([]interface{}, error) {
	// TODO detect unused args
	res := make([]interface{}, len(pb))
NEXT_BIND:
	for i, n := range pb {
		for _, a := range args {
			if a.Name == n {
				res[i] = a.Value
				continue NEXT_BIND
			}
		}
		return nil, fmt.Errorf("no argument for parameter '%s'", n)
	}
	return res, nil
}

type PositionRewriter string

func (pw PositionRewriter) Rewrite(t *goxic.Template) (
	ft *goxic.Template,
	b Binder,
	err error,
) {
	var bt goxic.BounT
	t.NewBounT(&bt)
	var phmap []string
	for i := 0; i <= t.FixCount(); i++ {
		ph := param(t.PhAt(i))
		if ph == "" {
			continue
		}
		phmap = append(phmap, ph)
		bt.Bind(content.P(pw), i)
	}
	ft, err = bt.Fixate()
	if ft == nil {
		return t, PositionBinder(phmap), err
	}
	return ft, PositionBinder(phmap), err
}
