package gxsql

import (
	"database/sql"
	"fmt"

	"git.fractalqb.de/fractalqb/goxic"
	"git.fractalqb.de/fractalqb/goxic/content"
)

type IndexBinder []string

func (ib IndexBinder) Bind(args ...sql.NamedArg) ([]interface{}, error) {
	if len(ib) != len(args) {
		return nil, fmt.Errorf("expect %d args, got %d", len(ib), len(args))
	}
	res := make([]interface{}, len(ib))
NEXT_BIND:
	for i, n := range ib {
		for _, a := range args {
			if a.Name == n {
				res[i] = a.Value
				continue NEXT_BIND
			}
		}
		return nil, fmt.Errorf("no argument for parameter '%s'", n)
	}
	return res, nil
}

type IndexRewriter struct {
	Format string
	Start  int
}

func (iw IndexRewriter) Rewrite(t *goxic.Template) (
	ft *goxic.Template,
	b Binder,
	err error,
) {
	var bt goxic.BounT
	t.NewBounT(&bt)
	phidxs := make(map[string]int)
	var phmap []string
	for i := 0; i <= t.FixCount(); i++ {
		ph := param(t.PhAt(i))
		if ph == "" {
			continue
		}
		pi, ok := phidxs[ph]
		if !ok {
			pi = len(phidxs)
			phidxs[ph] = pi
			phmap = append(phmap, ph)
		}
		bt.Bind(content.Fmt(iw.Format, iw.Start+pi), i)
	}
	ft, err = bt.Fixate()
	if ft == nil {
		return t, IndexBinder(phmap), err
	}
	return ft, IndexBinder(phmap), err
}
