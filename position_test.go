package gxsql

import (
	"database/sql"
	"fmt"

	"git.fractalqb.de/fractalqb/goxic"
	. "git.fractalqb.de/fractalqb/goxic/content"
)

func ExamplePosition() {
	prw := PositionRewriter("?")
	stmt, bnd := MustRewrite(prw, exampleTemplate, map[string]goxic.Content{
		"schema": P("public."),
	})
	fmt.Println("binder:", bnd)
	fmt.Println(stmt)
	fmt.Println(bnd.Bind(
		sql.Named("nm", "Doe"),
		sql.Named("age", 42),
	))
	// Output:
	// binder: [nm age nm]
	// SELECT * FROM public.users WHERE name = ? AND age < ? OR given = ?
	// [Doe 42 Doe] <nil>
}
