// Package gxsql handles idiosyncrasies of different SQL dialects through the
// use of templates.
//
// This shall not encourage to inject external input into the text form of SQL
// statements! Instead gxsql helps to uniquely hanlde the robust assignment of
// named arguments to different conventions for SQL placeholders.
//
// Additionally one can dynamically create SQL statements through the use of
// the underlying goxic template engine. If doing so, care hase to be taken to
// not introduce security risks.
//
// E.g. to ge these two different statemens from one source
//
//    SELECT name, age FROM person WHERE id=?
//    SELECT name, age FROM pim.person WHERE id=$1
//
// one would start with a unified statement template that has a simple template
// placeholder :schema: and a parameter placeholder :$id:
//
//    SELECT name, age FROM :schema:person WHERE id=:$id:
//
// Depending on the chosen ParamRewriter one can easily create different
// versions of the statement.
package gxsql

import (
	"bytes"
	"database/sql"
	"fmt"
	"strings"

	"git.fractalqb.de/fractalqb/goxic"
)

func NewParser() *goxic.Parser {
	res := goxic.NewParser(":", ":", "--", "")
	return res
}

type Binder interface {
	Bind(args ...sql.NamedArg) ([]interface{}, error)
}

type ParamRewriter interface {
	Rewrite(t *goxic.Template) (*goxic.Template, Binder, error)
}

type Statement struct {
	SQL  string
	Args Binder
	Log  func(method, sql string, args []interface{})
}

func (stmt *Statement) Rewrite(prw ParamRewriter, tmpl string, args map[string]goxic.Content) error {
	var err error
	stmt.SQL, stmt.Args, err = Rewrite(prw, tmpl, args)
	return err
}

func (stmt *Statement) MustRewrite(prw ParamRewriter, tmpl string, args map[string]goxic.Content) {
	stmt.SQL, stmt.Args = MustRewrite(prw, tmpl, args)
}

func (stmt *Statement) Exec(db *sql.DB, args ...sql.NamedArg) (sql.Result, error) {
	bound, err := stmt.Args.Bind(args...)
	if err != nil {
		return nil, err
	}
	if stmt.Log != nil {
		stmt.Log("Exec", stmt.SQL, bound)
	}
	return db.Exec(stmt.SQL, bound...)
}

func (stmt *Statement) ExecTx(tx *sql.Tx, args ...sql.NamedArg) (sql.Result, error) {
	bound, err := stmt.Args.Bind(args...)
	if err != nil {
		return nil, err
	}
	if stmt.Log != nil {
		stmt.Log("ExecTx", stmt.SQL, bound)
	}
	return tx.Exec(stmt.SQL, bound...)
}

func (stmt *Statement) Query(db *sql.DB, args ...sql.NamedArg) (*sql.Rows, error) {
	bound, err := stmt.Args.Bind(args...)
	if err != nil {
		return nil, err
	}
	if stmt.Log != nil {
		stmt.Log("Query", stmt.SQL, bound)
	}
	return db.Query(stmt.SQL, bound...)
}

func (stmt *Statement) QueryTx(tx *sql.Tx, args ...sql.NamedArg) (*sql.Rows, error) {
	bound, err := stmt.Args.Bind(args...)
	if err != nil {
		return nil, err
	}
	if stmt.Log != nil {
		stmt.Log("QueryTx", stmt.SQL, bound)
	}
	return tx.Query(stmt.SQL, bound...)
}

type Scanner interface {
	Scan(dest ...interface{}) error
}

type scanError struct {
	error
}

func (se scanError) Scan(dest ...interface{}) error { return se }

func (stmt *Statement) QueryRow(db *sql.DB, args ...sql.NamedArg) Scanner {
	bound, err := stmt.Args.Bind(args...)
	if err != nil {
		return scanError{err}
	}
	if stmt.Log != nil {
		stmt.Log("QueryRow", stmt.SQL, bound)
	}
	return db.QueryRow(stmt.SQL, bound...)
}

func (stmt *Statement) QueryRowTx(tx *sql.Tx, args ...sql.NamedArg) Scanner {
	bound, err := stmt.Args.Bind(args...)
	if err != nil {
		return scanError{err}
	}
	if stmt.Log != nil {
		stmt.Log("QueryRowTx", stmt.SQL, bound)
	}
	return tx.QueryRow(stmt.SQL, bound...)
}

func Rewrite(prw ParamRewriter, tmpl string, args map[string]goxic.Content) (string, Binder, error) {
	tmpls := make(map[string]*goxic.Template)
	err := ParseString(tmpl, "sql", tmpls)
	if err != nil {
		return "", nil, err
	}
	rwt, binder, err := prw.Rewrite(tmpls[""])
	if err != nil {
		return "", nil, err
	}
	if rwt.PhCount() == 0 {
		return string(rwt.Static()), binder, nil
	}
	var bt goxic.BounT
	rwt.NewBounT(&bt)
	for _, ph := range rwt.Phs() {
		cnt := args[ph]
		if cnt == nil {
			return "", nil, fmt.Errorf("no content for placeholder '%s'", ph)
		}
		bt.BindName(cnt, ph)
	}
	var sb strings.Builder
	if _, err = bt.WriteTo(&sb); err != nil {
		return "", nil, err
	}
	return strings.TrimSpace(sb.String()), binder, nil
}

func MustRewrite(prw ParamRewriter, tmpl string, args map[string]goxic.Content) (string, Binder) {
	s, b, err := Rewrite(prw, tmpl, args)
	if err != nil {
		panic(err)
	}
	return s, b
}

func ParseString(s, rootName string, into map[string]*goxic.Template) error {
	return defaultParser.ParseString(s, rootName, into)
}

var (
	defaultParser = NewParser()
)

func init() {
	defaultParser.PrepLine = bytes.TrimSpace
	defaultParser.Endl = " "
}

func param(ph string) string {
	if len(ph) > 1 && ph[0] == '$' {
		return ph[1:]
	}
	return ""
}
