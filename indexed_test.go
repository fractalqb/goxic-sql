package gxsql

import (
	"database/sql"
	"fmt"

	"git.fractalqb.de/fractalqb/goxic"
	. "git.fractalqb.de/fractalqb/goxic/content"
)

func ExampleIndexed() {
	irw := IndexRewriter{Format: "$%d", Start: 1}
	stmt, bnd := MustRewrite(irw, exampleTemplate, map[string]goxic.Content{
		"schema": P("public."),
	})
	fmt.Println("binder:", bnd)
	fmt.Println(stmt)
	fmt.Println(bnd.Bind(
		sql.Named("nm", "Doe"),
		sql.Named("age", 42),
	))
	// Output:
	// binder: [nm age]
	// SELECT * FROM public.users WHERE name = $1 AND age < $2 OR given = $1
	// [Doe 42] <nil>
}
